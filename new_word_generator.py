import numpy as np

#You can play with these 5 parameters 
#and don't need to change funtions and functions parameters.
#parameters of the model:
#gramm_cat: 'v', 'n', 'adj', 'adv'
#n_gramms: 1, 2, 3 (bug if more than 3)
n_gramm_size = 1
min_length_generated_word = 5
max_length_generated_word = 15
gramm_cat = 'n'
number_generated_word = 20


class NGramm:

    def __init__(self, Letters: str, ID: int, Count: list):
        self.id_matrix = ID
        self.letters = Letters
        self.count = Count

#replace special char
def replace_char_accent(cmt, word):
    new_word = word
    for index_l in range(len(word)):
        letter = word[index_l]
        char_to_replace = [ "à", "ù", "ï", "î", "â", "œ", "ê", "ü", "ö", "ç", "û", "ô", "ë"]
        new_char = ["a", "u", "i", "i", "a", "o", "e", "u", "o", "c", "u", "o", "e"]
        letter_noAccent = letter
        for e in range(len(char_to_replace)):
            if letter == char_to_replace[e]:
                letter_noAccent = letter.replace(char_to_replace[e], new_char[e])
                new_word[index_l] = letter_noAccent
    return new_word

#create list of words
def word_list(cat):
    file = ""
    if cat == "n":
        file = "finaldataNouns.txt"
    elif cat == 'v':
        file = 'finalDataVerbs.txt'
    elif cat == 'adv':
        file = 'finalDataAdv.txt'
    elif cat == 'adj':
        file = 'finalDataAdj.txt'
    with open(file, 'r', encoding='utf8') as f:
        lines = f.read()
        word_list = lines.replace('$', '-$_')
        word_list = word_list.split('$')
        return  word_list

def create_n_gramm(word_letters: list, n:int, p0: int):
    n_gramm = ""
    for e in range(p0, p0 - n, -1):
        n_gramm = word_letters[e] + n_gramm
    return n_gramm

#this dict contains a n_gramm as a key and an NGramm object as value
#NGramm.count countains the probability distributions

def init_dict_count(corpus: list, n: int):
    count = {}
    start = NGramm('_', 0, [0]*256)
    count[start.letters] = start
    for word in corpus:
        letters = list(word)
        letters = replace_char_accent(letters)
        for e in range(len(letters)-1):
            if e == 0:
                count['_'].count[ord(letters[1])] += 1
                for i in range(n-2):
                    n_gramm = NGramm(letters[1+i], len(count), [0] * 256)
                    if n_gramm.letters not in count:
                        count[n_gramm.letters] = n_gramm
                    count[n_gramm.letters].count[ord(letters[2+i])] += 1
            if e >= n-1:
                n_gramm = create_n_gramm(letters, n, e)
                n_gramm = NGramm(n_gramm, len(count), [0]*256)
                if n_gramm.letters not in count:
                    count[n_gramm.letters] = n_gramm
                count[n_gramm.letters].count[ord(letters[e+1])] += 1
    return count

def init_transition_matrix(occ_count: dict):
    transition_matrix = np.zeros((len(occ_count), 256))
    for n_gramm in occ_count:
        for e in range(256):
            transition_matrix[occ_count[n_gramm].id_matrix, e] = occ_count[n_gramm].count[e]
    return  transition_matrix

def sum_line(line, matrix):
    sum = 0
    for e in range(np.shape(matrix)[1]):
        sum = sum + matrix[line, e]
    return sum

#change sum of occurences to percent
def nb_to_prob(matrix):
    for e in range(np.shape(matrix)[0]):
        sum = sum_line(e, matrix)
        for i in range(np.shape(matrix)[1]):
            if sum == 0:
                matrix[e, i] = 0
                print(e)
            else:
                matrix[e,i] = matrix[e,i] / sum


def generate_word(matrix_proba, occ: dict, n_gramm: int, min_length_new_word: int, max_length_new_word: int):
    word = "_"
    i = 0
    while word[-1] != "-":
        previous_letter = i
        new_letter = np.random.choice(range(np.shape(matrix_proba)[1]), p=matrix_proba[i,:])
        word = word + chr(new_letter)

        if word[-1] != '-' and len(word) >= n_gramm:
            new_n_gramm =  create_n_gramm(list(word), n_gramm, len(word)-1)
            i = occ[new_n_gramm].id_matrix

        elif len(word) < n_gramm:
            i = occ[word[-1]].id_matrix

        elif len(word) > max_length_new_word + 2:
            word = "_"
            i = 0
    if len(word) < min_length_new_word + 2:
        generate_word(matrix_proba, occ, n_gramm_size)
    else:
        word = word.replace("_", "")
        word = word.replace("-", "")
        print(word)
    return word

def generate_n_logatoms(cat: str, n: int, n_gramm):

    words = word_list(cat)
    count = init_dict_count(words, n_gramm)
    matrix = init_transition_matrix(count)
    nb_to_prob(matrix)
    print('_________'+ cat +'________')
    for i in range(n):
        incr = 0
        generate_word(matrix, count, n_gramm, incr)

generate_n_logatoms(gramm_cat, number_generated_word, n_gramm_size)
